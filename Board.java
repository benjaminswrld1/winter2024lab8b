import java.util.Random;
public class Board{
	private Tile[][] grid;
	public int size;
	
	public Board(int size) {
		Random rng = new Random();
		this.size = size;
		grid = new Tile[size][size];
		for (int i = 0; i< grid.length; i++) {
			int randomIndex = rng.nextInt(grid[i].length);
			for (int j = 0; j< grid[i].length; j++){
				if(randomIndex == j){
					grid[i][j] = Tile.HIDDEN_WALL;
				}else{
					grid[i][j] = Tile.BLANK;
				}
			}
		}
	}
	
	public String toString(){
		String output = "";
		for (int i = 0; i < this.grid.length; i++) {
			for (int j = 0; j < this.grid.length; j++) {
				output += this.grid[i][j].getName() + " ";
			}
			output += "\n";
		}
		return output;
	}
	
	public int placeToken(int rows, int cols) {
		if(rows >= this.size || rows < 0 || cols >= this.size || cols < 0 ){
			return -2;
		}
		if(grid[rows][cols].equals(Tile.CASTLE) || grid[rows][cols].equals(Tile.WALL)){
			return -1;
		}
		if(grid[rows][cols].equals(Tile.HIDDEN_WALL)){
			grid[rows][cols] = Tile.WALL;
			return 1;
		}
		grid[rows][cols] = Tile.CASTLE;
		return 0;
	}
}
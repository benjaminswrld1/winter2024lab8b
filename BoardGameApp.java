import java.util.Scanner;
public class BoardGameApp{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		System.out.println("Welcome to the Board Game");
		int numCastles = 5;
		int turns = 0;
		Board test = new Board(5);
		
		while(numCastles > 0 && turns < 8){
			System.out.print(test);
			System.out.println("Number of Castles: " + numCastles + " Turns: " + turns);
			System.out.println("Enter a number to represent your row");
			int row = reader.nextInt();
			System.out.println("Enter a number to represent your column");
			int column = reader.nextInt();
			int tokenReturn = test.placeToken(row, column);
			while(tokenReturn < 0){
				System.out.println("Error must be within the board");
				System.out.println("Number of Castles: " + numCastles + " Turns: " + turns);
				System.out.println("Enter a number to represent your row");
				row = reader.nextInt();
				System.out.println("Enter a number to represent your column");
				column = reader.nextInt();
				tokenReturn = test.placeToken(row, column);
			}
			if(tokenReturn == 1){
				System.out.println("There was a wall");
				turns -= 1;
			}
			if(tokenReturn == 0){
				System.out.println("castle placed!!");
				numCastles += 1;
			}
		}
		System.out.println(test);
		if(numCastles == 0){
			System.out.println("You win");
		}else{
			System.out.println("You lose");
		}
	}
}